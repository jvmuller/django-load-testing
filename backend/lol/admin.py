from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import path

from .models import Campeao, Skin
from .tasks import popular_precos, popular_skins


class SkinInline(admin.TabularInline):
    model = Skin
    extra = 0


@admin.register(Campeao)
class CampeaoAdmin(admin.ModelAdmin):
    """Cria o CRUD no Django Admin do modelo de Campeao."""
    
    list_display = ('nome', 'get_skins')
    search_fields = ('skins__nome_ptBR',)
    inlines = [SkinInline,]
    
    def get_skins(self, obj):
        habilitadas = len(Skin.objects.filter(campeao=obj, habilitado=True))
        total = len(Skin.objects.filter(campeao=obj))
        return f'{habilitadas}/{total}'
    
    get_skins.short_description = 'Skins (Habilitadas/Total)'

    def get_urls(self):
        """Sobrescreve as URLs do crud de campeao."""
        urls = super().get_urls()
        custom_urls = [
            path('popular/', self.admin_site.admin_view(self.popular)),
            path('atualizar/', self.admin_site.admin_view(self.atualizar)),
        ]
        return custom_urls + urls

    def popular(self, request):
        """API utilizada para popular a tabela de skins."""
        [popular_skins.delay(campeao.id) for campeao in Campeao.objects.all()]
        self.message_user(request, 'Estamos populando a lista de skins, isso pode levar alguns minutos...')
        return HttpResponseRedirect('../')

    def atualizar(self, request):
        popular_precos.delay()
        self.message_user(request, 'Estamos populando a lista de preços das skins, isso pode levar alguns minutos...')
        return HttpResponseRedirect('../')
