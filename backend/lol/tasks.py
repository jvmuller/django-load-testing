import requests
from datetime import datetime
from conf.celery import app

from .models import Campeao, Skin
import logging
from bs4 import BeautifulSoup

def altera_nome_enUS(x):
    skin = Skin.objects.get(id=x['id'])
    skin.nome_enUS = x['name']
    return skin

@app.task
def popular_skins(campeao_id):
    if len(Skin.objects.all()) == 0:
        response_ptBR = requests.get(f'https://ddragon.leagueoflegends.com/cdn/12.11.1/data/pt_BR/champion/{campeao_id}.json')
        response_enUS = requests.get(f'https://ddragon.leagueoflegends.com/cdn/12.11.1/data/en_US/champion/{campeao_id}.json')
        data_ptBR = response_ptBR.json()['data'][campeao_id]['skins']
        data_enUS = response_enUS.json()['data'][campeao_id]['skins']
        
        champion = Campeao.objects.get(id=campeao_id)
        skins_ptBR = [Skin(id=x['id'], nome_ptBR=x['name'], campeao=champion) for x in data_ptBR if x['name'] != 'default']
        Skin.objects.bulk_create(skins_ptBR)
        
        skins_enUS = [altera_nome_enUS(x) for x in data_enUS if x['name'] != 'default']
        Skin.objects.bulk_update(skins_enUS, ['nome_enUS'])

@app.task
def popular_precos():
    index = 1
    update_queries = []
    response = requests.get('https://leagueoflegends.fandom.com/wiki/List_of_champion_skins_(League_of_Legends)')
    if response.status_code == 200:
        soup = BeautifulSoup(response.content, 'html.parser')
        rows = soup.find('table').find('tbody').find_all('tr')
        
        while len(rows) > index:            
            try:
                nome_enUS = rows[index].find_all('td')[1].get_text()
                data = rows[index].find_all('td')[3].get_text()
                valor_rp = rows[index].find_all('td')[4].get_text()
                index += 1
                
                skin = Skin.objects.get(nome_enUS=nome_enUS)
                skin.data = None if data == 'N/A' else datetime.strptime(data, '%d-%b-%Y')
                skin.valor_rp = None if valor_rp == 'special' else valor_rp
                update_queries.append(skin)
            except Skin.DoesNotExist as e:
                logging.warning(e)
                continue
            
        Skin.objects.bulk_update(update_queries, ['data', 'valor_rp'])