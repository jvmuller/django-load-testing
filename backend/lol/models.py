from django.db import models


class Campeao(models.Model):
    """Instância um campeão de League of Legends no banco de dados."""
    
    id = models.CharField('ID', max_length=32, unique=True)
    key = models.PositiveIntegerField('Key', primary_key=True)
    nome = models.CharField('Nome', max_length=64)

    class Meta:
        verbose_name = 'Campeão'
        verbose_name_plural = 'Campeões'
        ordering = ['nome']
    
    def __str__(self):
        """Método que retorna o nome do campeão ao retornar objeto."""
        return self.nome


class Skin(models.Model):
    """Instância uma skin de League of Legends no banco de dados."""
    
    id = models.PositiveIntegerField('ID', primary_key=True)
    habilitado = models.BooleanField('Habilitado', default=False)
    nome_ptBR = models.CharField('Nome (pt_BR)', max_length=64)
    nome_enUS = models.CharField('Nome (en_US)', max_length=64)
    valor_rp = models.IntegerField('Valor (RP)', null=True, blank=True)
    percentual_desconto = models.IntegerField('Desconto (%)', null=True, blank=True)
    data = models.DateField('Data de lançamento', null=True, blank=True)
    campeao = models.ForeignKey(Campeao, on_delete=models.CASCADE, related_name='skins')
    
    class Meta:
        verbose_name = 'Skins'
        verbose_name_plural = 'Skins'
        ordering = ['nome_ptBR']
    
    def __str__(self):
        """Método que retorna o nome da skin ao retornar objeto."""
        return self.nome_ptBR
