from django.db import models

    
class Anime(models.Model):
    """Instância um anime no banco de dados."""
        
    id = models.PositiveBigIntegerField('ID', primary_key=True, help_text='É o mesmo utilizado para identificação no MyAnimeList')
    nome = models.CharField('Nome do anime', max_length=255)
    quantidade_de_votos = models.PositiveBigIntegerField('Votos', null=True, blank=True, help_text='Quantidade de usuários do MyAnimeList que classificaram o anime')
    score_myanimelist = models.DecimalField('Score MyAnimeList', decimal_places=2, max_digits=4, null=True, blank=True)
    
    class Meta:
        verbose_name = 'Anime'
        verbose_name_plural = 'Animes'
        ordering = ['-score_myanimelist', '-quantidade_de_votos']

    def __str__(self):
        """Método que retorna o nome do anime ao retornar objeto."""
        return self.nome
