from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import path

from .models import Anime
from .tasks import atualiza_anime, popular_animes


@admin.register(Anime)
class AnimeAdmin(admin.ModelAdmin):
    """Cria o CRUD no Django Admin do modelo de Anime."""
    
    list_display = ('id', 'nome', 'score_myanimelist', 'quantidade_de_votos')
    search_fields = ('nome',)
    
    def get_urls(self):
        """Sobrescreve as URLs do crud de anime para adicionar a função que sincroniza com listagem
        dos animes do MyAnimeList."""
        urls = super().get_urls()
        custom_urls = [
            path('popular/', self.admin_site.admin_view(self.popular)),
            path('atualizar/', self.admin_site.admin_view(self.atualizar)),
        ]
        return custom_urls + urls

    def popular(self, request):
        """API utilizada para popular a tabela de animes."""
        popular_animes.delay()
        self.message_user(request, 'Estamos populando a lista de animes, isso pode levar alguns minutos...')
        return HttpResponseRedirect('../')

    def atualizar(self, request):
        """API utilizada para atualizar os dados cadastrados com os dados do MyAnimeList."""
        [atualiza_anime.delay(id_myanimelist=anime.id) for anime in Anime.objects.filter(score_myanimelist__isnull=True)]
        self.message_user(request, 'Estamos sincronizando a lista de animes, isso pode levar alguns minutos...')
        return HttpResponseRedirect('../')
