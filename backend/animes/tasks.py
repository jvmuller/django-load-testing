import logging

import requests
from bs4 import BeautifulSoup
from conf.celery import app

from .models import Anime


@app.task
def popular_animes():
    """Adiciona animes existentes do MyAnimeList ao banco de dados utilizando web scraping."""
    limit = 0
    page = requests.get(f'https://myanimelist.net/topanime.php?limit={limit}')
    status_code = page.status_code
    while status_code == 200:
        soup = BeautifulSoup(page.content, 'html.parser')
        animes = soup.find_all('h3', class_='anime_ranking_h3')
        for anime in animes:
            id = anime.a['href'].split('/')[4]
            nome = anime.text.strip()
            if not Anime.objects.filter(id=id).exists():
                Anime.objects.create(id=id, nome=nome)
        limit += 50
        page = requests.get(f'https://myanimelist.net/topanime.php?limit={limit}')
        status_code = page.status_code
        

@app.task(autoretry_for=(Exception,), retry_kwargs={'default_retry_delay': 300, 'max_rentries': 3})
def atualiza_anime(id_myanimelist):
    """Atualiza um anime do banco de dados utilizando técnica de web scraping."""
    try:
        anime = Anime.objects.get(id=id_myanimelist)
        page = requests.get(f'https://myanimelist.net/anime/{id_myanimelist}')
        if page.status_code == 200:
            soup = BeautifulSoup(page.content, 'html.parser')
            ref_nome = soup.find('h1', class_='title-name')
            ref_score = soup.find('div', class_='score-label')
    
            nome = ref_nome.text.strip()
            score_myanimelist = ref_score.text.strip()
            
            if anime.nome != nome:
                anime.nome = nome
            
            if anime.score_myanimelist != score_myanimelist:
                if score_myanimelist != 'N/A':
                    anime.score_myanimelist = score_myanimelist
                else:
                    anime.score_myanimelist = 0
    
            anime.quantidade_de_votos = 0
            
            anime.save()
    except Anime.DoesNotExist:
        logging.warning(f'O anime {id_myanimelist} não existe no banco de dados')
