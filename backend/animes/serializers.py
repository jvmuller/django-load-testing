from rest_framework import serializers
from .models import Anime


class AnimeSerializer(serializers.ModelSerializer):
    """Serializa um objeto de Anime em JSON."""
    
    class Meta:
        model = Anime
        fields = '__all__'
