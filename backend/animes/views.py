from rest_framework import viewsets

from .models import Anime
from .serializers import AnimeSerializer


class AnimeViewSet(viewsets.ModelViewSet):
    """API relacionada ao modelo Anime."""
    
    queryset = Anime.objects.all()
    serializer_class = AnimeSerializer
